

using UnrealBuildTool;
using System.Collections.Generic;

public class TicTacToeTarget : TargetRules
{
    public TicTacToeTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("TicTacToe");
	}
}
