// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TicTacToeBlock.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"

ATicTacToeBlock::ATicTacToeBlock()
{
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/TicTacToe/Meshes/TicTacToeCube.TicTactoeCube"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(1.f, 1.f, 0.25f));
	BlockMesh->SetupAttachment(DummyRoot);
}


void ATicTacToeBlock::SetBlockMeterial(UMaterial * material) {
	BlockMesh->SetMaterial(0, material);	
}
