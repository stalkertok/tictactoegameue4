

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UObject/ConstructorHelpers.h"
#include "TicTacToeBlock.generated.h"


UCLASS(minimalapi)
class ATicTacToeBlock : public AActor
{
	GENERATED_BODY()

	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* BlockMesh;

	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;

public:
	ATicTacToeBlock();

	UFUNCTION()
	void SetBlockMeterial(UMaterial * material);


	FORCEINLINE class UStaticMeshComponent* GetBlockMesh() const { return BlockMesh; }
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
};



