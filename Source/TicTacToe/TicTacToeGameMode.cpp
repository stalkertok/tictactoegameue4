

#include "TicTacToeGameMode.h"
#include "TicTacToePlayerController.h"
#include "TicTacToePawn.h"

ATicTacToeGameMode::ATicTacToeGameMode(){

	DefaultPawnClass = ATicTacToePawn::StaticClass();
	PlayerControllerClass = ATicTacToePlayerController::StaticClass();

	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> XMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> OMaterial;
		FConstructorStatics()
			:BaseMaterial(TEXT("/Game/TicTacToe/Meshes/BaseMaterial.BaseMaterial")),
			XMaterial(TEXT("/Game/TicTacToe/Meshes/XMaterial.XMaterial")),
			OMaterial(TEXT("/Game/Tictactoe/Meshes/OMaterial.OMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	XMaterial = ConstructorStatics.XMaterial.Get();
	OMaterial = ConstructorStatics.OMaterial.Get();

	static ConstructorHelpers::FClassFinder<UUserWidget> WidgetAsset(TEXT("/Game/TicTacToe/MainMenu"));

	WidgetClass = WidgetAsset.Class;

	//������ �������� ����
	Size = 5;
	//������ ������������ ��� �����
	BlockSpacing = 280.f;

	TTTGameField= TicTacToeGameField(Size);

}

void ATicTacToeGameMode::BeginPlay() {

	Super::BeginPlay();

	isGameOver = false;

	Player = TicTacToeGameField::X;
	AI = TicTacToeGameField::O;

	TTTGamePlayer.SetType(AI);
	TTTGamePlayer.setTypeEnemy(Player);

	TTTGameField.Clear();
	
	createMainMenu();

	GenerateBloks();

	setBaseMaterialBlocks();

	Update();
}

void ATicTacToeGameMode::SetItem(int32 row, int32 column){

	if (TTTGameField.IsItemEmpty(row, column)) {
		TTTGameField.SetItem(row, column, Player);
		Update();
	}
}

void ATicTacToeGameMode::Update(){
	if (isGameOver)
		return;

	TTTGamePlayer.NextStep(TTTGameField);

	for (auto& Elem : Blocks)
		switch (TTTGameField.GetItem(Elem.Value.Key, Elem.Value.Value)) {
		case TicTacToeGameField::Type::X:
			Elem.Key->SetBlockMeterial(XMaterial);
			break;
		case TicTacToeGameField::Type::O:
			Elem.Key->SetBlockMeterial(OMaterial);
			break;

		}

	if (TTTGameField.IsFullLineExist(Player)) {
			isGameOver = true;
			ResultText->SetText(FText::FromString(FString(TEXT("You win!"))));
			MainMenu->SetVisibility(ESlateVisibility::Visible);
		return;
	}

	if (TTTGameField.IsFullLineExist(AI)) {
		isGameOver = true;
		ResultText->SetText(FText::FromString(FString(TEXT("You Lose!"))));
		MainMenu->SetVisibility(ESlateVisibility::Visible);
		return;
	}
	if (TTTGameField.IsFullGameField()) {
		isGameOver = true;
		UE_LOG(LogTemp, Warning, TEXT("1_____"));
		ResultText->SetText(FText::FromString(FString(TEXT("No Winner!"))));
		UE_LOG(LogTemp, Warning, TEXT("1"));
		MainMenu->SetVisibility(ESlateVisibility::Visible);
		UE_LOG(LogTemp, Warning, TEXT("2"));

		return;
	}
}

void ATicTacToeGameMode::OnClickNewGame(){
	BeginPlay();
}

void ATicTacToeGameMode::GenerateBloks(){

	const int32 NumBlocks = Size * Size;

	//������� ������� ���� �� ������

	auto isExistBlock = false;

	for (int32 BlockIndex = 0; BlockIndex < NumBlocks; BlockIndex++) {

		isExistBlock = false;

		auto Row = BlockIndex / Size;
		auto Column = BlockIndex % Size;
		const float XOffset = Column * BlockSpacing - (Size-1)*BlockSpacing / 2;
		const float YOffset = Row * BlockSpacing - (Size-1)*BlockSpacing / 2;

		const FVector BlockLocation = FVector(XOffset, YOffset, 0.f);

		//��������� ���� ���� � ����� ����
		for (const auto &Elem : Blocks)
			if (Elem.Value.Key == Row && Elem.Value.Value == Column && Elem.Key != nullptr)
				isExistBlock = true;

		//���� ���, �� �������
		if (!isExistBlock){
		ATicTacToeBlock* NewBlock = GetWorld()->SpawnActor<ATicTacToeBlock>(BlockLocation, FRotator(0, 0, 0));

		if (NewBlock != nullptr)
			Blocks.Add(NewBlock, TPair<int32, int32>{Row, Column});
	}
	}

}

void ATicTacToeGameMode::setBaseMaterialBlocks(){
	for (auto& Elem : Blocks)
		Elem.Key->SetBlockMeterial(BaseMaterial);
}

void ATicTacToeGameMode::createMainMenu(){
	if (WidgetClass)
	{
		if (MainMenu == nullptr) {
			MainMenu = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);
			MainMenu->AddToViewport();
		}

		auto WidgetTree = MainMenu->WidgetTree;

		if (ResultText==nullptr)
			ResultText = Cast<UTextBlock>(WidgetTree->FindWidget(FName("ResultText")));

		if (NewGameButton==nullptr) {
			NewGameButton = Cast<UButton>(WidgetTree->FindWidget(FName("NewGameButton")));
			NewGameButton->OnClicked.AddDynamic(this, &ATicTacToeGameMode::OnClickNewGame);
		}
	}

	if (MainMenu!=nullptr) {
		MainMenu->SetVisibility(ESlateVisibility::Hidden);
	}
}

TPair<int32, int32> ATicTacToeGameMode::GetPosition(const ATicTacToeBlock * block) {
	return Blocks[block];
}

