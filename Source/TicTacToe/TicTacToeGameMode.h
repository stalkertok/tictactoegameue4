

#pragma once

#include "tictactoegame.h"
#include "TicTacToeBlock.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TextBlock.h"
#include "Button.h"
#include "WidgetTree.h"
#include "GameFramework/GameModeBase.h"
#include "TicTacToeGameMode.generated.h"


UCLASS(minimalapi)
class ATicTacToeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		int32 Size;

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		float BlockSpacing;

	//��������� ��� ����
	UPROPERTY()
		class UMaterial* BaseMaterial;
	UPROPERTY()
		class UMaterial* XMaterial;
	UPROPERTY()
		class UMaterial* OMaterial;

	UPROPERTY()
	class UUserWidget *MainMenu;

	UFUNCTION()
		void OnClickNewGame();

	TPair<int32, int32> GetPosition(const ATicTacToeBlock* block);
	void SetItem(int32 row, int32 column);

protected:
	TSubclassOf<class UUserWidget> WidgetClass;

	class UTextBlock* ResultText;
	class UButton* NewGameButton;

	//������������� ����� � ������� � tmap
	void GenerateBloks();
	void setBaseMaterialBlocks();
	//������� ���� � ��������� �� ����, ������ � �����
	void createMainMenu();

	ATicTacToeGameMode();

	bool isGameOver=false;

	//������ ��������� �� ���� � ��������� � ������� ���� � ���� ������ � �������
	TMap <ATicTacToeBlock*, TPair<int32, int32>>  Blocks;

	TicTacToeGameField TTTGameField;
	
	//����� ������������� �� Pawn
	TicTacToeGamePlayer TTTGamePlayer;

	TicTacToeGameField::Type Player;
	TicTacToeGameField::Type AI;

	virtual void BeginPlay() override;

	void Update();

};



