#include "TicTacToePawn.h"
#include "TicTacToeBlock.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "TicTacToeGameMode.h"

ATicTacToePawn::ATicTacToePawn(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

void ATicTacToePawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

}

void ATicTacToePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	//���������� ������� �� ���� �����
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAction("Click", IE_Pressed, this, &ATicTacToePawn::OnClick);
}

void ATicTacToePawn::CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult)
{
	Super::CalcCamera(DeltaTime, OutResult);

}


//�������� �������� �� ����
void ATicTacToePawn::OnClick(){
	FHitResult Hit = FHitResult(ForceInit);
	GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECollisionChannel::ECC_Pawn, true, Hit);
	if (Hit.Actor.IsValid())
	{
		ATicTacToeBlock*  BlockGrid = Cast<ATicTacToeBlock>(Hit.GetActor());
			ATicTacToeGameMode* GameMode = (ATicTacToeGameMode*)(GetWorld()->GetAuthGameMode());
			//�������� ������ ������ � �������
			auto Pos = GameMode->GetPosition(BlockGrid);
			//������������� ���� �����
			GameMode->SetItem(Pos.Key,Pos.Value);
	}
}