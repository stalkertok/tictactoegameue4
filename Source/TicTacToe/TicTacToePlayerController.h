

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TicTacToePlayerController.generated.h"


UCLASS()
class ATicTacToePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATicTacToePlayerController();

	virtual void BeginPlay() override;
};


