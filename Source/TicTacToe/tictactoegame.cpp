#include "tictactoegame.h"

TicTacToeGameField::TicTacToeGameField(int32 gameFiledSize):Size(gameFiledSize){
    this->GameFiled.SetNum(Size);

    for (auto & row: GameFiled){
        row.SetNum(Size);
        for (auto & item: row)
            item=this->EMPTY;
    }
}

int32 TicTacToeGameField::GetSize() const
{
    return Size;
}

bool TicTacToeGameField::IsFullHorizontalLine(Type type)const{

    for(auto row=0; row<Size;row++){

        if (HorizontalLineCount(row,type)==Size)
            return true;
    }

    return false;

}

bool TicTacToeGameField::IsFullverticalLine(Type type)const{

    for(auto col=0; col<Size;col++){
        if (VerticalLineCount(col,type)==Size)
            return true;
    }

    return false;
}

bool TicTacToeGameField::IsFullDiagonallLine(Type type)const{

    if (DiagonalLineCount(DiagonaleType::Main,type)==Size)
        return true;

    if (DiagonalLineCount(DiagonaleType::Side,type)==Size)
        return true;

    return false;

}

bool TicTacToeGameField::IsFullGameField() const{
    auto count=0;
    for(auto row=0; row<Size;row++)
        for(auto col=0; col<Size;col++)
            if (GameFiled[row][col]!=EMPTY)
                count ++;
    return  (count==Size*Size)?true:false;

}

bool TicTacToeGameField::IsItemEmpty(int32 row, int32 column) const{
    return !GameFiled[row][column];
}

bool TicTacToeGameField::IsEmpty() const{
    auto count=0;
    for(auto row=0; row<Size;row++)
        for(auto col=0; col<Size;col++)
            if (GameFiled[row][col]==EMPTY)
                count ++;
    return  (count==Size*Size)?true:false;
}

int32 TicTacToeGameField::HorizontalLineCount(int32 row, TicTacToeGameField::Type type) const{
    auto count = 0;
    for(auto col=0; col<Size;col++)
        if (GameFiled[row][col]==type)
            count ++;

    return count;
}

int32 TicTacToeGameField::VerticalLineCount(int32 column, Type type) const{
    auto count = 0;
    for(auto row=0; row<Size;row++)
        if (GameFiled[row][column]==type)
            count ++;

    return count;
}

int32 TicTacToeGameField::DiagonalLineCount(TicTacToeGameField::DiagonaleType dType, TicTacToeGameField::Type type) const{

    auto count=0;
    switch (dType) {

    case DiagonaleType::Main:
        for(auto col=0; col<Size;col++)
            if (GameFiled[col][col] == type)
                ++count;
        return count;
    case   DiagonaleType::Side:
        for(auto col=0; col<Size;col++)
            if (GameFiled[col][Size-col-1] == type)
                ++count;
        return count;
    }
	return 0;
}

bool TicTacToeGameField::IsFullLineExist(Type type) const{
    return IsFullverticalLine(type)|IsFullDiagonallLine(type)|IsFullHorizontalLine(type);
}

TicTacToeGameField::Type TicTacToeGameField::GetItem(int32 row, int32 column)const{
    return this->GameFiled[row][column];
}

void TicTacToeGameField::Clear(){

    for(auto row=0; row<Size;row++)
        for(auto col=0; col<Size;col++)
            GameFiled[row][col]=EMPTY;
}

void TicTacToeGameField::SetItem(int32 row,int32 column,TicTacToeGameField::Type type){
    this->GameFiled[row][column]=type;
}

void TicTacToeGameField::SetEmptyItemHorisontal(int32 row, TicTacToeGameField::Type type){
    for (auto col=0;col<Size;col++)
        if (GameFiled[row][col]==TicTacToeGameField::EMPTY){
            GameFiled[row][col]=type;
            return;
        }
}

void TicTacToeGameField::SetEmptyItemVertical(int32 column, TicTacToeGameField::Type type){
    for (auto row=0;row<Size;row++){
        if (GameFiled[row][column]==TicTacToeGameField::EMPTY){
            GameFiled[row][column]=type;
            return;
        }
    }

}

void TicTacToeGameField::SetEmptyItemDiagonale(TicTacToeGameField::DiagonaleType dType, TicTacToeGameField::Type type){
    switch (dType) {

    case DiagonaleType::Main:
        for(auto col=0; col<Size;col++)
            if (GameFiled[col][col] == TicTacToeGameField::EMPTY){
                GameFiled[col][col]=type;
                return;}
        break;
    case   DiagonaleType::Side:
        for(auto col=0; col<Size;col++)
            if (GameFiled[col][Size-col-1] == TicTacToeGameField::EMPTY){
                GameFiled[col][Size-col-1]=type;
                return;
            }
        break;
    }
}

TicTacToeGamePlayer::TicTacToeGamePlayer(){
}

TicTacToeGamePlayer::TicTacToeGamePlayer(TicTacToeGameField::Type type, TicTacToeGameField::Type typeEnemy):Type(type),TypeEnemy(typeEnemy){
}

void TicTacToeGamePlayer::NextStep(TicTacToeGameField &gameFiled){

    //Если игровое поле пустое, то ставим метку в центр
    if (gameFiled.IsEmpty()){
        gameFiled.SetItem(gameFiled.GetSize()/2,gameFiled.GetSize()/2,Type);
        return;
    }

	TArray<int32> countsRowRayting;
	TArray<int32> countsColRayting;

	countsColRayting.SetNum(gameFiled.GetSize());
	countsRowRayting.SetNum(gameFiled.GetSize());

    int32 DiagonaleMainRaiting = 0;
    int32 DiagonaleSideRaiting = 0;

    auto countsRow=0;
    auto countsCol=0;
    auto countsRowEnemy=0;
    auto countsColEnemy=0;

    for (auto i=0;i<gameFiled.GetSize();i++){

        //считаем количество для строк и колонок, компьютера и его противника
        countsRow=gameFiled.HorizontalLineCount(i,Type);
        countsCol=gameFiled.VerticalLineCount(i,Type);
        countsRowEnemy=gameFiled.HorizontalLineCount(i,TypeEnemy);
        countsColEnemy=gameFiled.VerticalLineCount(i,TypeEnemy);

        //считаем рейтинг
		countsColRayting[i] = GetRayting(countsCol, countsColEnemy, gameFiled.GetSize());
        countsRowRayting[i]=GetRayting(countsRow,countsRowEnemy,gameFiled.GetSize());
    }

    //то же самое для главной диагонали
    auto CountDiagonaleMain = gameFiled.DiagonalLineCount(TicTacToeGameField::Main,Type);
    auto CountDiagonaleMainEnemy = gameFiled.DiagonalLineCount(TicTacToeGameField::Main,TypeEnemy);

    DiagonaleMainRaiting = GetRayting(CountDiagonaleMain,CountDiagonaleMainEnemy,gameFiled.GetSize());

    //то же самое для побочной диагонали
    auto CountDiagonaleSide = gameFiled.DiagonalLineCount(TicTacToeGameField::Side,Type);
    auto CountDiagonaleSideEnemy = gameFiled.DiagonalLineCount(TicTacToeGameField::Side,TypeEnemy);

    DiagonaleSideRaiting = GetRayting(CountDiagonaleSide,CountDiagonaleSideEnemy,gameFiled.GetSize());

	//ищем максимальный рейтин среди строк, запоминаем в какой строке
	int32 max_r_index;
	auto max_r = FMath::Max(countsRowRayting,&max_r_index);

	//ищем максимальный рейтин среди колонок, запоминаем в какой колонке
	int32 max_c_index; 
	auto max_c = FMath::Max(countsColRayting,&max_c_index);


    TPair<TicTacToeGameField::DiagonaleType,int32> DiagonaleRaiting;

    //максимальный рейтинг для диагоналей, выбираем из них максимальный, запоминаем в какой диагонали
    if (DiagonaleMainRaiting>=DiagonaleSideRaiting)
        DiagonaleRaiting=TPair<TicTacToeGameField::DiagonaleType, int32>(TicTacToeGameField::Main,DiagonaleMainRaiting);
    else
        DiagonaleRaiting= TPair<TicTacToeGameField::DiagonaleType, int32>(TicTacToeGameField::Side, DiagonaleSideRaiting);

    //Если максимум в строке, ставим метку в этой строке, в первую не пустуюю ячейку
    if (max_r>=max_c && max_r>=DiagonaleRaiting.Value){
        gameFiled.SetEmptyItemHorisontal(max_r_index,Type);
        return;
    }
    //Если максимум в колонке, ставим метку в этой колонке, в первую не пустуюю ячейку
    if (max_c>=max_r && max_c>=DiagonaleRaiting.Value){
        gameFiled.SetEmptyItemVertical(max_c_index,Type);
        return;
    }
    //Если максимум в диагонале, ставим метку в этой диагонали, в первую не пустуюю ячейку
    if (DiagonaleRaiting.Value>=max_r && DiagonaleRaiting.Value>=max_c){
        gameFiled.SetEmptyItemDiagonale(DiagonaleRaiting.Key,Type);
        return;
    }
}

void TicTacToeGamePlayer::SetType(const TicTacToeGameField::Type &type){
    Type = type;
}

TicTacToeGameField::Type TicTacToeGamePlayer::GetType() const{
    return Type;
}

TicTacToeGameField::Type TicTacToeGamePlayer::GetTypeEnemy() const
{
    return TypeEnemy;
}

void TicTacToeGamePlayer::setTypeEnemy(const TicTacToeGameField::Type &typeEnemy)
{
    TypeEnemy = typeEnemy;
}

int32 TicTacToeGamePlayer::GetRayting(int32 count, int32 countEnemy, int32 sizeField){

    //Если строка или колонка заполнена, то минимальный рейтинг
    if (count+countEnemy==sizeField)
        return 0;

    //Если противник может выиграть, то мешаем ему
    if (countEnemy>=sizeField-1)
        return  2+sizeField+countEnemy;
    //Если компьютер может выиграть, выигрываем, при этом рейтин выиграша выше чем рейтинг проиграша в следующем ходу
    if (count>=sizeField-1)
        return  2+2*sizeField+count;//чтобы был больше чем 1, и чем проигрыш соперника
    //Если не можем выиграть, то увеличиваем количество меток компьютера,
    //ставим там где нет метки соперника и количество меток компьютера максимальное
    if (count>=0 && countEnemy==0)
        return  2+count;//чтобы был больше чем 1

    //в остальных случаех нет разницы куда ставить метку
    return 1;

}
