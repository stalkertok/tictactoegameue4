#ifndef TICTACTOEGAME_H
#define TICTACTOEGAME_H
#include "CoreMinimal.h"

//������� ���� � ���� ����������� TARRAY
class  TicTacToeGameField{
public:
    TicTacToeGameField(int32 Size=5);
    enum Type{
        EMPTY=0,
        O=1,
        X=2
    };

    enum DiagonaleType{
        Main=0,
        Side=1
    };

    int32 GetSize() const;
    bool IsFullLineExist(Type Type) const;
    void SetItem(int32 Row, int32 Column, Type Type);
    void SetEmptyItemHorisontal(int32 Row, Type Type);
    void SetEmptyItemVertical(int32 Column, Type Type);
    void SetEmptyItemDiagonale(DiagonaleType DType, Type Type);
    Type GetItem(int32 Row, int32 Column)const;
    void Clear();

    bool IsFullGameField()const;
    bool IsItemEmpty(int32 Row,int32 Column)const;
    bool IsEmpty()const;

    int32 HorizontalLineCount(int32 Row,Type Type)const;
    int32 VerticalLineCount(int32 Column,Type Type)const;
    int32 DiagonalLineCount(DiagonaleType DType,Type Type)const;

private:
    int32 Size;
    TArray <TArray<Type>> GameFiled;

    bool IsFullHorizontalLine(Type Type)const;
    bool IsFullverticalLine(Type Type)const;
    bool IsFullDiagonallLine(Type Type)const;
};

class TicTacToeGamePlayer
{
public:
	TicTacToeGamePlayer();
    TicTacToeGamePlayer(TicTacToeGameField::Type Type,TicTacToeGameField::Type TypeEnemy);
    void NextStep(TicTacToeGameField & GameFiled);

    void SetType(const TicTacToeGameField::Type &Type);

    TicTacToeGameField::Type GetType() const;

    TicTacToeGameField::Type GetTypeEnemy() const;
    void setTypeEnemy(const TicTacToeGameField::Type &TypeEnemy);

private:
    TicTacToeGameField::Type Type;
    TicTacToeGameField::Type TypeEnemy;

    int32 GetRayting(int32 Count,int32 CountEnemy,int32 SizeField);
};

#endif // TICTACTOEGAME_H
