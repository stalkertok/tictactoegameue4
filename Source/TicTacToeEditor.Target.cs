

using UnrealBuildTool;
using System.Collections.Generic;

public class TicTacToeEditorTarget : TargetRules
{
    public TicTacToeEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("TicTacToe");
	}
}
